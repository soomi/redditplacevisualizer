This program simulates a timelapse of the creation of reddit's r/place experiment, where over a million users came together to create something unique using pixel drawing.

The program reads data from a file containing all tile placements that were done, and outputs it on the screen in the form of pixels.

Each row contains a timestamp, xy coordinates, color and hashed name of the user who placed it.

Controls
SCROLL to zoom
Click and drag on the canvas to move it around
Hold Left/Right Arrow keys to slow/speed up the timelapse
Hold LCTRL to speed up the timelapse control
Hold LSHIFT to slow down the timelapse control
ENTER to pause

Required libraries
SFML 2.4.2