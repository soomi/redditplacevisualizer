import csv

print("Shortening time...")

filename = "TestLab/tile_placements.csv";

with open(filename) as fin, open("output.csv", "w") as f:
    reader = csv.reader(fin, delimiter=",")
    writer = csv.writer(f)

    for row in reader:
        if (row):
            row[0] = row[0][:-3];
            writer.writerow(row)
			
print("Success")


