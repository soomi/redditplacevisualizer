import csv
import operator

print("Sorting by timestamp...")

with open("input.csv") as fin, open("output.csv", "w") as f:
    reader = csv.reader(fin, delimiter=",")
    writer = csv.writer(f)
    header = next(reader)  # <--- Pop header out
    writer.writerow(header) # <--- Write header
    sortedlist = sorted(reader, key=lambda row: int(row[0]))
    writer.writerows(sortedlist)

print("Success")


