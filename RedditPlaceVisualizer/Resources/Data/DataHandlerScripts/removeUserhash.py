import csv

print("Removing user_hash...")

filename = "tile_placements.csv";

with open(filename) as fin, open("output.csv", "w") as f:
    reader = csv.reader(fin, delimiter=",")
    writer = csv.writer(f)

    for row in reader:
        if (row):
            row.pop(1)
            writer.writerow(row)
			
print("Success")


