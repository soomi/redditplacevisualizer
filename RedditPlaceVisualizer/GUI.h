#pragma once
#include "SFML\Graphics.hpp"

class RenderManager;

class GUI
{
private:
	sf::Font m_font;
	sf::Text m_text_StatusInfo;
	sf::Text m_text_InputInfo;
	sf::RectangleShape m_background;

	RenderManager* m_pRenderManager;
	
	std::string formatTime(float seconds);
public:
	GUI();

	void initialize();
	void render(sf::RenderTarget* pRenderTarget);
	void update(const float deltaTime);

	~GUI();
};

