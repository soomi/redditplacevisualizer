#pragma once
#include "DataManager.h"
#include <SFML\Graphics.hpp>

class RenderManager
{
private:
	DataManager m_dataManager;

	sf::VertexArray m_canvasVertices;
	sf::Transform m_canvasTransform;

	int m_timestamp_firstTile;
	int m_timestamp_lastTile;
	int m_timestamp_current;

	unsigned m_currentTileIndex;

	float m_timeStampCounter;
	float m_updateSpeed;
	float m_keyHoldMultiplier;

	bool m_paused;
	bool m_hasLoadedFirstTile;
	bool m_isRenderingVertexArray;

	sf::Color getColorFromIndex(const int& index);

	void clearVertexArray();
public:
	RenderManager();

	DataManager& getDataManager() { return m_dataManager; }

	int getCurrentTimeStamp() const { return m_timestamp_current; }

	bool isPaused() const { return m_paused; }
	bool hasLoadedFirstTile() const { return m_hasLoadedFirstTile; }

	float getTimePassed() const { return float(m_timestamp_current - m_timestamp_firstTile); }
	float getUpdateSpeed() const { return m_updateSpeed; }

	void initialize();
	void update(const float dt);
	void handleEvent(sf::Event event);
	void render(sf::RenderTarget* renderTarget);
	void onFirstTileLoaded(const TileData& tile);
	void drawFromLoadedData(TileMap& tiles);

	~RenderManager();
};

