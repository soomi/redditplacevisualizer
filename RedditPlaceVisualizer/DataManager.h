#pragma once
#include <string>
#include <vector>

struct TileData
{
	int timestamp;
	int x;
	int y;
	int color;
};

typedef std::vector<TileData*> TileMap;

class RenderManager;
class DataManager
{
private:
	TileMap m_tiles;

	std::string m_fileName;
	std::string m_line;
	std::streampos m_filePosition;

	unsigned int m_numTotalTilesLoaded;

	bool m_hasReachedEnd;

	void onEndReached();
public:
	DataManager();

	bool hasReachedEndOfFile() const { return m_hasReachedEnd; }

	unsigned int getTotalNumTilesLoaded() const { return m_numTotalTilesLoaded; }
	unsigned int getCurrentNumTiles() const { return m_tiles.size(); }

	TileMap& getTiles() { return m_tiles; }

	void setFile(const std::string fileName);
	void loadTileCount(RenderManager& renderManager, const unsigned numTilesToLoad);
	void clearAllTiles();

	~DataManager();
};

