#include "stdafx.h"
#include "GUI.h"
#include "App.h"
#include "RenderManager.h"
#include <string>
#include <sstream>
#include <iomanip>

GUI::GUI()
{
}

void GUI::initialize()
{
	m_pRenderManager = App::getInstance()->getRenderManager();

	m_font.loadFromFile("Resources/Fonts/arial.ttf");

	m_text_StatusInfo.setFont(m_font);
	m_text_StatusInfo.setCharacterSize(15);
	m_text_StatusInfo.setFillColor(sf::Color::White);
	m_text_StatusInfo.setOutlineThickness(1.f);
	m_text_StatusInfo.setOutlineColor(sf::Color::Black);
	m_text_StatusInfo.setPosition(10.f, 5.f);

	m_text_InputInfo.setFont(m_font);
	m_text_InputInfo.setCharacterSize(13);
	m_text_InputInfo.setFillColor(sf::Color::White);
	m_text_InputInfo.setOutlineThickness(1.f);
	m_text_InputInfo.setOutlineColor(sf::Color::Black);
	m_text_InputInfo.setPosition(App::getInstance()->getWindow()->getSize().x - 300.f, 5.f);

	std::string inputInfo;
	inputInfo += "Scroll to zoom. Click and drag to move around.\n\n";
	inputInfo += "Left/Right Arrow keys - Modify timelapse speed\n";
	inputInfo += "Enter - Pause the timelapse\n";
	inputInfo += "R - Reset camera position\n";
	inputInfo += "T - Toggle UI Visibility\n";
	inputInfo += "Y - Capure canvas and save to file\n";

	m_text_InputInfo.setString(inputInfo);

	m_background.setSize(sf::Vector2f(360.f, 30.f));
	m_background.setFillColor(sf::Color(0, 0, 0, 140));
	m_background.setPosition(0.f, 0.f);
}

std::string GUI::formatTime(float seconds)
{
	float timeVal = seconds;

	std::string suffix = "Second";

	if (timeVal >= 3600.f) // 1 hour = 3600 seconds
	{
		suffix = "Hour";

		timeVal /= 3600.f;
	}
	else if (timeVal >= 60.f) // 1 minute = 60 seconds
	{
		suffix = "Minute";

		timeVal /= 60.f;
	}

	suffix.insert(0, " ");

	suffix += "s";

	std::string timeValStr;
	timeValStr = std::to_string(timeVal);
	timeValStr = timeValStr.substr(0, timeValStr.find(".") + 2);

	std::string timeStr = timeValStr + suffix;

	return timeStr;
}

void GUI::render(sf::RenderTarget* pRenderTarget)
{
	pRenderTarget->draw(m_background);
	pRenderTarget->draw(m_text_StatusInfo);
	pRenderTarget->draw(m_text_InputInfo);
}

void GUI::update(const float deltaTime)
{
	// display status text with some info
	std::stringstream textSS;
	textSS << formatTime(m_pRenderManager->getTimePassed()) << " | ";
	textSS << "Speed " << std::fixed << std::setprecision(1) << m_pRenderManager->getUpdateSpeed() << "x | ";
	textSS << "Status: ";

	if (m_pRenderManager->getDataManager().hasReachedEndOfFile())
	{
		textSS << "Finished";
	}
	else if (m_pRenderManager->isPaused())
	{
		textSS << "Paused";
	}
	else
	{
		textSS << "Running";
	}

	m_text_StatusInfo.setString(textSS.str());
}

GUI::~GUI()
{
}
