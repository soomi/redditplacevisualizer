#pragma once
#include <SFML/Graphics.hpp>
#include "DataManager.h"
#include "RenderManager.h"
#include "GUI.h"

class App
{
private:
	static App* m_pInstance;

	sf::RenderWindow m_window;

	sf::RenderTexture m_canvasTexture;

	sf::Vector2f m_lastMouseMovedPosition;

	sf::View m_mainCamera;
	sf::View m_guiCamera;

	DataManager m_dataManager;
	RenderManager m_renderManager;
	GUI m_gui;

	bool m_renderGui;

	float m_cameraZoomAmount;
	float m_cameraZoomAmountCounter;

	int m_lastScrollDelta;

	void render();
	void update(const float deltaTime);
	void handleEvent(sf::Event event);
	void zoomCamera(float zoomAmount);
	void saveCurrentCanvasToFile(std::string fileName);
public:
	App();

	sf::RenderWindow* getWindow() { return &m_window; }

	DataManager* getDataManager() { return &m_dataManager; }
	RenderManager* getRenderManager() { return &m_renderManager; }
	GUI* getGui() { return &m_gui; }

	static App* getInstance() { return m_pInstance; }

	void run();

	~App();
};

