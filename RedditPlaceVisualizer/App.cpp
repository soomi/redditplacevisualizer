#include "stdafx.h"
#include "App.h"
#include <Windows.h>

App* App::m_pInstance = nullptr;

const unsigned int screenWidth = 1000;
const unsigned int screenHeight = 1000;

const unsigned int canvasWidth = 1000;
const unsigned int canvasHeight = 1000;

App::App() :
	m_renderGui(true),
	m_cameraZoomAmount(0.f),
	m_lastScrollDelta(1)
{
	if (!m_pInstance)
	{
		m_pInstance = this;
	}
	else
	{
		return;
	}

	m_window.create(sf::VideoMode(screenWidth, screenHeight), "Reddit Place Simulator", sf::Style::Titlebar | sf::Style::Close);
	m_window.setKeyRepeatEnabled(false);

	m_guiCamera = m_window.getView();
	m_mainCamera = sf::View(sf::FloatRect(0.f, 0.f, float(screenWidth), float(screenHeight)));

	m_canvasTexture.create(screenWidth, screenHeight);
	m_canvasTexture.setView(m_mainCamera);
	m_canvasTexture.clear(sf::Color::White);

	m_renderManager.initialize();
	m_gui.initialize();
}

void App::render() // Render
{
	m_window.setView(m_mainCamera);
	m_renderManager.render(&m_canvasTexture);

	m_canvasTexture.display();
	m_window.draw(sf::Sprite(m_canvasTexture.getTexture()));

	if (m_renderGui)
	{
		m_window.setView(m_guiCamera);
		m_gui.render(&m_window);
	}
}

void App::update(const float deltaTime) // Update
{
	if (m_cameraZoomAmountCounter != 0.f)
	{
		float amount = m_cameraZoomAmount * deltaTime * 4.f;
		m_cameraZoomAmountCounter += amount * -1.f;
		m_mainCamera.zoom(1.f + amount);

		if ((amount < 0.f && m_cameraZoomAmountCounter > 0.f) || (amount > 0.f && m_cameraZoomAmountCounter < 0.f))
			m_cameraZoomAmountCounter = 0.f;
	}

	m_renderManager.update(deltaTime);
	m_gui.update(deltaTime);
}

void App::saveCurrentCanvasToFile(std::string fileName) // Save canvas to file
{
	CreateDirectory(L"SavedImages", NULL);

	sf::Image canvasImage = m_canvasTexture.getTexture().copyToImage();
	std::string savePath("SavedImages/" + fileName + ".jpg");
	if (canvasImage.saveToFile(savePath))
	{
		printf("Saved current canvas to %s\n", savePath.c_str());
	}
	else
	{
		printf("Error capturing canvas!\n");
	}
}

void App::handleEvent(sf::Event event)
{
	m_renderManager.handleEvent(event);

	if (event.type == sf::Event::Closed)
	{
		m_window.close();
	}
	else if (event.type == sf::Event::KeyPressed)
	{
		switch (event.key.code)
		{
		case sf::Keyboard::R:
			m_mainCamera = sf::View(sf::FloatRect(0.f, 0.f, float(screenWidth), float(screenHeight)));
			break;
		case sf::Keyboard::T:
			m_renderGui = !m_renderGui;
			break;
		case sf::Keyboard::Y:
			saveCurrentCanvasToFile("Canvas_" + std::to_string(m_renderManager.getCurrentTimeStamp()));
			break;
		}
	}
	else if (event.type == sf::Event::MouseMoved)
	{
		sf::Vector2f mousePos(sf::Mouse::getPosition());
		sf::Vector2f mouseDelta(m_lastMouseMovedPosition - mousePos);

		m_lastMouseMovedPosition = mousePos;

		float currentZoom = m_mainCamera.getSize().x / screenWidth;
		mouseDelta *= currentZoom;
		if (sf::Mouse::isButtonPressed(sf::Mouse::Left))
		{
			m_mainCamera.move(mouseDelta);
		}
	}
	else if (event.type == sf::Event::MouseWheelMoved)
	{
		int scrollDelta = event.mouseWheel.delta;
		if (scrollDelta != m_lastScrollDelta)
		{
			m_cameraZoomAmount = 0.f;
			m_cameraZoomAmountCounter = 0.f;

			m_lastScrollDelta = scrollDelta;
		}

		if (scrollDelta < 1)
		{
			zoomCamera(0.1f);
		}
		else
		{
			zoomCamera(-0.1f);
		}
	}
}

void App::zoomCamera(float zoomAmount) // Zoom camera
{
	m_cameraZoomAmount += zoomAmount;
	m_cameraZoomAmountCounter += zoomAmount;
}

void App::run() // App loop
{
	sf::Clock clock;

	while (m_window.isOpen())
	{
		float deltaTime = clock.restart().asSeconds();

		sf::Event event;
		while (m_window.pollEvent(event))
		{
			handleEvent(event);
		}

		update(deltaTime);

		m_window.clear();
		render();
		m_window.display();
	}
}

App::~App()
{
}
