#include "stdafx.h"
#include "DataManager.h"
#include "RenderManager.h"
#include <fstream>
#include <sstream>

#define TIMESTAMP_EVENTSTART 1490972400

DataManager::DataManager() :
	m_numTotalTilesLoaded(0),
	m_hasReachedEnd(false)
{

}

void DataManager::setFile(std::string fileName) // Set file to load from
{
	m_fileName = fileName;

	printf("File set to %s\n", fileName.c_str());
}

void DataManager::onEndReached() // end of file reached
{
	printf("Reached end of file.\n");
}

void DataManager::loadTileCount(RenderManager& renderManager, const unsigned numTilesToLoad) // loads x tiles
{
	if (!m_hasReachedEnd)
	{
		printf("Loading %i tiles.\n", numTilesToLoad);

		clearAllTiles();
		m_tiles = std::vector<TileData*>();

		// open file
		std::ifstream file(m_fileName, std::ios::binary);

		// go to last position in the file
		file.seekg(m_filePosition);

		int iterateCount = 0;
		int columnIndex = 0;
		int dataIndex = 0;
		std::stringstream ss;
		std::string column;

		TileData* pTile = nullptr;

		while (std::getline(file, m_line))
		{
			ss << m_line;

			if (m_numTotalTilesLoaded != 0)
			{
				pTile = new TileData;

				dataIndex = 0;
				while (std::getline(ss, column, ','))
				{
					int columnValue = std::stoi(column);
					switch (dataIndex)
					{
					case 0:
						pTile->timestamp = columnValue;
						break;
					case 1:
						pTile->x = columnValue;
						break;
					case 2:
						pTile->y = columnValue;
						break;
					case 3:
						pTile->color = columnValue;
						break;
					}

					dataIndex++;
				}

				// add tile to vector
				m_tiles.push_back(pTile);

				// start date
				if (!renderManager.hasLoadedFirstTile() && pTile->timestamp >= TIMESTAMP_EVENTSTART)
				{
					renderManager.onFirstTileLoaded(*pTile);
				}
			}

			m_numTotalTilesLoaded++;

			if (iterateCount == numTilesToLoad)
			{
				// save current position in file
				m_filePosition = file.tellg();
				break;
			}

			iterateCount++;
			ss.clear();
		}

		// no tile was loaded - end of file reached
		if (iterateCount == 0)
		{
			m_hasReachedEnd = true;
			onEndReached();
		}
	}
}

void DataManager::clearAllTiles() // Delete and clear tiles
{
	unsigned vSize = m_tiles.size();
	for (unsigned i = 0; i < vSize; i++)
	{
		delete m_tiles[i];
	}

	m_tiles.clear();
}

DataManager::~DataManager()
{
	clearAllTiles();
}
