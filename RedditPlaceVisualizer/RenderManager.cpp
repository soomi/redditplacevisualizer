#include "stdafx.h"
#include "RenderManager.h"

#define CANVAS_NUM_POINTS 1000001
#define MAX_TILES_PER_LOAD 10000

RenderManager::RenderManager() :
	m_updateSpeed(750.f),
	m_keyHoldMultiplier(0.f),
	m_timeStampCounter(0.f),
	m_timestamp_firstTile(0),
	m_timestamp_lastTile(0),
	m_timestamp_current(0),
	m_currentTileIndex(0),
	m_paused(false),
	m_hasLoadedFirstTile(false),
	m_isRenderingVertexArray(true)
{
	m_canvasVertices = sf::VertexArray(sf::PrimitiveType::Points, CANVAS_NUM_POINTS);
}

sf::Color RenderManager::getColorFromIndex(const int& index)
{
	switch (index)
	{
	case 0:
		return sf::Color(255, 255, 255);
	case 1:
		return sf::Color(228, 228, 228);
	case 2:
		return sf::Color(136, 136, 136);
	case 3:
		return sf::Color(34, 34, 34);
	case 4:
		return sf::Color(255, 167, 209);
	case 5:
		return sf::Color(229, 0, 0);
	case 6:
		return sf::Color(229, 149, 0);
	case 7:
		return sf::Color(160, 106, 66);
	case 8:
		return sf::Color(229, 217, 0);
	case 9:
		return sf::Color(148, 224, 68);
	case 10:
		return sf::Color(2, 190, 1);
	case 11:
		return sf::Color(0, 229, 240);
	case 12:
		return sf::Color(0, 131, 199);
	case 13:
		return sf::Color(0, 0, 234);
	case 14:
		return sf::Color(224, 74, 255);
	case 15:
		return sf::Color(130, 0, 128);
	}

	return sf::Color();
}

void RenderManager::initialize()
{
	m_dataManager.setFile("Resources/Data/tile_placements.csv"); // set file
}

void RenderManager::drawFromLoadedData(TileMap& tiles) // Place relevant tiles on the canvas
{
	const unsigned int maxTilesPerFrame = int(1.f + m_updateSpeed * 0.5f);
	
	TileData* pTile = nullptr;
	unsigned int tilesAdded = 0;

	unsigned numTiles = tiles.size();
	for (unsigned i = m_currentTileIndex; i < numTiles; i++)
	{
		pTile = tiles[i];

		if (pTile->timestamp <= m_timestamp_current && tilesAdded < maxTilesPerFrame)
		{
			sf::Vector2i position(pTile->x, pTile->y);
			int positionIndex = position.x * position.y;

			// set position and color
			m_canvasVertices[positionIndex].position = static_cast<sf::Vector2f>(position);
			m_canvasVertices[positionIndex].color = getColorFromIndex(pTile->color);

			m_currentTileIndex = i;

			tilesAdded++;
		}
		else
		{
			break;	
		}
	}

	if (pTile)
	{
		m_timestamp_lastTile = pTile->timestamp;
		m_timestamp_current = m_timestamp_lastTile;
	}
}

void RenderManager::clearVertexArray() // Destroy the canvas
{
	m_canvasVertices = sf::VertexArray();
	m_isRenderingVertexArray = false;
}

void RenderManager::update(const float dt) // Update
{
	// arrow keys control the update speed
	float keyHoldIncrease = dt * 120.f;
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::LShift))
	{
		keyHoldIncrease *= 0.1f;
	}
	else if (sf::Keyboard::isKeyPressed(sf::Keyboard::LControl))
	{
		keyHoldIncrease *= 15.f;
	}

	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Left))
	{
		m_updateSpeed -= dt * m_keyHoldMultiplier;

		m_keyHoldMultiplier += keyHoldIncrease;
	}
	else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Right))
	{
		m_updateSpeed += dt * m_keyHoldMultiplier;

		m_keyHoldMultiplier += keyHoldIncrease;
	}

	if (m_updateSpeed < 1.f) m_updateSpeed = 1.f;

	if (m_paused)
		return;

	// update canvas
	if (m_isRenderingVertexArray)
	{
		// update current timestamp
		m_timeStampCounter += dt * m_updateSpeed;
		if (m_timeStampCounter >= 1.f)
		{
			float value = std::floor(m_timeStampCounter);
			m_timeStampCounter -= value;

			m_timestamp_current += int(value);
		}

		// load in chunks of x rows
		if (m_currentTileIndex + 1 >= m_dataManager.getCurrentNumTiles())
		{
			int tilesToLoad = 150 * int(m_updateSpeed);
			if (tilesToLoad > MAX_TILES_PER_LOAD) tilesToLoad = MAX_TILES_PER_LOAD;
			m_dataManager.loadTileCount(*this, tilesToLoad);

			m_currentTileIndex = 0;
		}

		drawFromLoadedData(m_dataManager.getTiles());

		// destroy the canvas if end of file has been reached
		if (m_dataManager.hasReachedEndOfFile() && m_dataManager.getCurrentNumTiles() == 0)
		{
			clearVertexArray();

			printf("Loaded and rendered a total of %i tiles.\n", m_dataManager.getTotalNumTilesLoaded());
		}
	}
}

void RenderManager::handleEvent(sf::Event event) // Event handler
{
	if (event.type == sf::Event::KeyReleased)
	{
		if (event.key.code == sf::Keyboard::Left || event.key.code == sf::Keyboard::Right)
		{
			m_keyHoldMultiplier = 0.f;
		}
	}
	else if (event.type == sf::Event::KeyPressed)
	{
		if (event.key.code == sf::Keyboard::Return)
		{
			m_paused = !m_paused;
		}
	}
}

void RenderManager::render(sf::RenderTarget* pRenderTarget) // Render to render target
{
	if (m_isRenderingVertexArray)
		pRenderTarget->draw(m_canvasVertices, m_canvasTransform);
}

void RenderManager::onFirstTileLoaded(const TileData& tile)
{
	m_timestamp_firstTile = tile.timestamp;
	m_timestamp_lastTile = m_timestamp_firstTile;
	m_timestamp_current = m_timestamp_lastTile;

	m_hasLoadedFirstTile = true;
}

RenderManager::~RenderManager()
{

}
